<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
    <meta name="robots" content="noindex,nofollow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/assets/css/app.min.css?{{ time() }}">
    <link rel="icon" type="image/png"  href="/favicon.ico">
    @yield('head')
</head>
<body>

    <div class="wrapper" id="app">
        @include('includes.sidebar')
        @include('includes.nav')

        <div id="page-content-wrapper">
            @yield('body')
        </div>
    </div>

    <script src="/assets/js/manifest.js"></script>
    <script src="/assets/js/vendor.js"></script>
    <script src="/assets/js/app.min.js"></script>
    <script src="/assets/js/scripts.min.js"></script>
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    $(function(){
        $('.table tr[data-href]').each(function(){
            $(this).css('cursor','pointer').hover(
                function(){
                    $(this).addClass('active');
                },
                function(){
                    $(this).removeClass('active');
                }).click( function(){
                    document.location = $(this).attr('data-href');
                }
            );
        });
    });
    </script>
    @yield('js')

</body>
</html>
