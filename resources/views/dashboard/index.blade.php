@extends('template.layout')

@section('head')
    <title>@lang('app.dashboard')</title>
@endsection



@section('body')
    <section class="dashboard">

        <div class="container-fluid">

            {{-- Tiles  --}}
            <div class="tiles">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="tile dark">
                            <div class="icon"><i class="flaticon-speedo"></i></div>
                            <div class="title">3 @lang('app.ongoing_projects')</div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="tile purple">
                            <div class="icon"><i class="flaticon-check-square"></i></div>
                            <div class="title">5 @lang('app.complete_projects')</div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="tile red">
                            <div class="icon"><i class="flaticon-layers"></i></div>
                            <div class="title">29 @lang('app.task')s</div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="mt-40">
                <h4 class="title">@lang('app.recent_projects')</h4>

                <table class="table table-custom">
                    <colgroup>
                        <col class="col-sm-6">
                        <col class="col-sm-2">
                        <col class="col-sm-2">
                        <col class="col-sm-2">
                    </colgroup>
                    <thead>
                        <tr>
                            <th>@lang('app.project_title')</th>
                            <th>@lang('app.project_manager')</th>
                            <th>@lang('app.due_date')</th>
                            <th>@lang('app.progress')</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($projects as $p)
                            <tr data-href="/projects/6a5d78c2-37eb-4285-86d3-e4d124fc451d">
                                <td>
                                    {{ $p->title }}
                                </td>
                                <td>{{ $p->manager }}</td>
                                <td>{{ $p->date }}</td>
                                <td>@lang($p->status)</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>

        </div>

    </section>
@endsection
