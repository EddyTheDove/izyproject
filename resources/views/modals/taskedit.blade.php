<div class="modal task-modal fade" id="taskedit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-close">
                <button type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close">
                    <i class="flaticon-multiply"></i>
                </button>
            </div>


            <div class="modal-body">

                <h2>Evaluation preliminaire du cout de developement</h2>
                <div class="numbers">
                    <ul class="list-unstyled">
                        <li><i class="flaticon-calendar"></i> 15/07/2017 - 20/07/2017</li>
                        <li><i class="flaticon-funds"></i> 400 000 FCFA</li>
                        <li><i class="flaticon-users-genders"></i> 3 @lang('app.contributor')s</li>
                    </ul>

                    <ul class="list-inline users">
                        <li>
                            <i class="flaticon-multiply"></i> Max Emmanuel
                        </li>
                        <li>
                            <i class="flaticon-multiply"></i> Fotsoh Armel
                        </li>
                        <li>
                            <i class="flaticon-multiply"></i> Bill Douanla
                        </li>
                        <li class="blue">
                            <i class="flaticon-cross"></i> @lang('app.add')
                        </li>
                    </ul>
                </div>

                <h4>@lang('app.indicator')</h4>
                <div class="">
                    Niveau d'avancement
                    <div class="task-progress">
                        <div class="progress-number">45 %</div>
                        <div class="progress-bar">
                            <span class="current" style="width:45%"></span>
                        </div>
                    </div>
                </div>


                <h4 class="mt-40">@lang('app.objective')</h4>
                <div class="paragraph">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    Duis aute irure dolor in reprehenderit in voluptate velit
                    esse cillum dolore eu fugiat nulla pariatur. Excepteur
                    sint occaecat cupidatat non proident, sunt in culpa qui
                    officia deserunt mollit anim id est laborum.
                </div>

                <h4 class="mt-20">@lang('app.deliverable')s</h4>
                <div class="paragraph">
                     Neque porro quisquam est, qui dolorem ipsum quia dolor
                     sit amet, consectetur, adipisci velit, sed quia non
                     numquam eius modi tempora incidunt ut labore et dolore
                     magnam aliquam quaerat voluptatem.
                </div>


                <div class="mt-40 text-right">
                    <a href class="mr-10 red">
                        <i class="flaticon-trash"></i> Supprimer
                    </a>
                    <button class="btn btn-greener btn-lg">
                        <i class="flaticon-check"></i> @lang('app.finish')
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
