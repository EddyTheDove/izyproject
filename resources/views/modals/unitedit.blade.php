<div class="modal task-modal fade" id="unitedit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-close">
                <button type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close">
                    <i class="flaticon-multiply"></i>
                </button>
            </div>


            <div class="modal-body">

                <form class="form" >
                    <h4>Unité Organisationnelle</h4>
                    <div class="form-group">
                        <input type="text" class="form-control input-lg" value="Direction" required>
                    </div>
                </form>

                <h4>Responsable</h4>
                <div class="numbers">

                    <ul class="list-inline users">
                        <li>
                            <i class="flaticon-multiply"></i> Bill Douanla
                        </li>
                        <li class="blue">
                            <i class="flaticon-pencil"></i> Modifier
                        </li>
                    </ul>
                </div>




                <h4>Membres</h4>
                <div class="numbers">
                    <ul class="list-inline users">
                        <li>
                            <i class="flaticon-multiply"></i> Max Emmanuel
                        </li>
                        <li>
                            <i class="flaticon-multiply"></i> Fotsoh Armel
                        </li>
                        <li class="blue">
                            <i class="flaticon-cross"></i> @lang('app.add')
                        </li>
                    </ul>
                </div>


                <h4>Sous Unités</h4>
                <div class="numbers">
                    <ul class="list-unstyled users">
                        <li>
                            <i class="flaticon-bin"></i> Laboratoire d'analyse médicale
                        </li>
                        <li>
                            <i class="flaticon-bin"></i> Département Informatique
                        </li>

                        <div class="mt-10">
                            <a class="btn btn-greener">
                                <i class="flaticon-cross"></i> @lang('app.add')
                            </a>
                        </div>

                    </ul>
                </div>



                <div class="mt-40 text-right">
                    <a href class="mr-10 red">
                        <i class="flaticon-trash"></i> Supprimer
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
