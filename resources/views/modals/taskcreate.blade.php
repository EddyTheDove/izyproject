<div class="modal task-modal fade" id="taskcreate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-close">
                <button type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close">
                    <i class="flaticon-multiply"></i>
                </button>
            </div>


            <div class="modal-body">
                <h2>@lang('app.new_task')</h2>

                <form class="form">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control input-lg" placeholder="Titre de la tache">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Date de debut</label>
                                <input type="text" name="start_date" class="form-control input-lg" placeholder="15/07/2017">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Date de fin</label>
                                <input type="text" name="end_date" class="form-control input-lg" placeholder="15/07/2017">
                            </div>
                        </div>
                    </div>

                    <div class="numbers">
                        <h3>@lang('app.contributor')s</h3>
                        <ul class="list-inline users">
                            <li class="blue">
                                <i class="flaticon-cross"></i> @lang('app.add')
                            </li>
                        </ul>
                    </div>

                    <h3>@lang('app.indicator')</h3>
                    <div class="form-group">
                        <input type="checkbox" name="indicator" value="1"> Niveau d'avancement <br>
                        <input type="checkbox" name="indicator" value="1"> Ecart de duree <br>
                        <input type="checkbox" name="indicator" value="1"> Taux de retard <br>
                    </div>


                    <h3 class="mt-40">@lang('app.objective')</h3>
                    <div class="">
                        <textarea name="indicator" rows="4" class="form-control"></textarea>
                    </div>

                    <h3 class="mt-20">@lang('app.deliverable')s</h3>
                    <div class="">
                        <textarea name="indicator" rows="4" class="form-control"></textarea>
                    </div>


                    <div class="mt-40 text-right">
                        <button class="btn btn-greener input-lg" type="submit">
                            <i class="flaticon-check"></i> Ajouter la tache
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
