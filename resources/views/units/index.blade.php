@extends('template.layout')

@section('head')
    <title>@lang('app.org_units')</title>
@endsection



@section('body')
    <section class="teams">

        <div class="container-fluid">
            <h4 class="title">@lang('app.org_units')</h4>

            <div class="units">
                <a href="" class="btn btn-greener btn-lg mb-20">
                    <i class="flaticon-cross"></i>
                    Créer une unité
                </a>






                <div class="unit">
                    <div class="name dropdown">
                        <i class="flaticon-more-v dropdown-toggle" data-toggle="dropdown"></i>
                        Direction

                        <ul class="dropdown-menu">
                            <li>
                                <a href data-toggle="modal" data-target="#unitedit">Paramètres</a>
                            </li>
                            <li><a href="">Déplacer vers le haut</a></li>
                            <li><a href="">Déplacer vers le bas</a></li>
                        </ul>
                    </div>
                    <div class="responsable">
                        <i class="flaticon-user-material"></i> Resp: Dr Atekeng Isidore
                    </div>
                    <div class="responsable">
                        <i class="flaticon-users-genders"></i> 1 Membre
                    </div>
                </div>







                <div class="unit level-2">
                    <div class="name">
                        <i class="flaticon-more-v"></i>
                        Laboratoire d'analyse médicale
                    </div>
                    <div class="responsable">
                        <i class="flaticon-user-material"></i> Resp: Dr Bill Tchato
                    </div>
                    <div class="responsable">
                        <i class="flaticon-users-genders"></i> 7 Membres
                    </div>
                </div>

                <div class="unit level-2">
                    <div class="name">
                        <i class="flaticon-more-v"></i>
                        Département Informatique
                    </div>
                    <div class="responsable">
                        <i class="flaticon-user-material"></i> Resp: Emma Belibi
                    </div>
                    <div class="responsable">
                        <i class="flaticon-users-genders"></i> 13 Membres
                    </div>
                </div>

                <div class="unit level-3">
                    <div class="name">
                        <i class="flaticon-more-v"></i>
                        Developeurs
                    </div>
                    <div class="responsable">
                        <i class="flaticon-user-material"></i> Resp: Emma Belibi
                    </div>
                    <div class="responsable">
                        <i class="flaticon-users-genders"></i> 13 Membres
                    </div>
                </div>


                <div class="unit level-4">
                    <div class="name">
                        <i class="flaticon-more-v"></i>
                        Backend Developeurs
                    </div>
                    <div class="responsable">
                        <i class="flaticon-user-material"></i> Resp: Sandrine Tawali
                    </div>
                    <div class="responsable">
                        <i class="flaticon-users-genders"></i> 4 Membres
                    </div>
                </div>


                <div class="unit level-3">
                    <div class="name">
                        <i class="flaticon-more-v"></i>
                        Testeurs
                    </div>
                    <div class="responsable">
                        <i class="flaticon-user-material"></i> Resp: Arnauld Tchakounte
                    </div>
                    <div class="responsable">
                        <i class="flaticon-users-genders"></i> 4 Membres
                    </div>
                </div>


            </div>
        </div>

    </section>


    @include('modals.unitedit')
@endsection
