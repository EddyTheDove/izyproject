@extends('template.layout')

@section('head')
    <title>@lang('app.project_details')</title>
@endsection



@section('body')
    <section class="page">

        <div class="container-fluid">

            <div class="page-title">
                <h3>Developement d'un logiciel de suivi des projets de recherche</h3>
            </div>

            <div class="block mt-20">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#details">
                            <i class="flaticon-page"></i>
                            Details
                        </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#tasks">
                            <i class="flaticon-layers"></i>
                            @lang('app.task')s
                        </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#indicators">
                            <i class="flaticon-slides-h"></i>
                            @lang('app.indicator')s
                        </a>
                    </li>
                    <li class="hidden-xs">
                        <a data-toggle="tab" href="#gantt">
                            <i class="flaticon-chart-gantt"></i>
                            Gantt
                        </a>
                    </li>
                </ul>


                <div class="block-content tab-content">

                    {{-- Details  --}}
                    <div class="tab-pane fade in active"  id="details">
                        <div class="project-details">
                            <ul class="list-unstyled fs-16" style="line-height:28px;">
                                <li>
                                    <i class="flaticon-info mr-10"></i>
                                    @lang('app.status') : <span class="teal">En Cours</span>
                                </li>
                                <li>
                                    <i class="flaticon-calendar mr-10"></i>
                                    @lang('app.start_date') : <span class="teal">15/07/2017</span>
                                </li>
                                <li>
                                    <i class="flaticon-calendar mr-10"></i>
                                    @lang('app.due_date') : <span class="teal">15/09/2017 - [Dans 75 jours]</span>
                                </li>
                                <li>
                                    <i class="flaticon-funds mr-10"></i>
                                    Budget : <span class="teal">10 000 000 FCFA</span>
                                </li>
                                <li>
                                    <i class="flaticon-user-material mr-10"></i>
                                    @lang('app.project_manager') : <span class="teal">Max Emmauel</span>
                                </li>
                                <li class="mt-20 bold">
                                    <i class="flaticon-lock mr-10"></i>
                                    Autorisations
                                </li>
                            </ul>

                            <div class="numbers">
                                <ul class="list-inline users">
                                    <li>
                                        <i class="flaticon-multiply"></i> Max Emmanuel
                                    </li>
                                    <li>
                                        <i class="flaticon-multiply"></i> Fotsoh Armel
                                    </li>
                                    <li>
                                        <i class="flaticon-multiply"></i> Bill Douanla
                                    </li>
                                    <li class="blue">
                                        <i class="flaticon-cross"></i> @lang('app.add')
                                    </li>
                                </ul>
                            </div>



                            <hr>
                            <div class="">
                                <h3>@lang('app.project_objectives')</h3>
                                <div class="mt-10 fs-16 teal">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </div>
                            </div>


                            <div class="mt-30">
                                <h3>@lang('app.expected_results')</h3>
                                <div class="mt-10 fs-16 teal">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </div>
                            </div>

                            <div class="mt-30 pb-40">
                                <h3>@lang('app.project_deliverables')</h3>
                                <div class="mt-10 teal">
                                    <ul class="ml-20">
                                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                                        <li>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</li>
                                        <li>Ut enim ad minim veniam</li>
                                        <li>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>



                    {{-- Tasks  --}}
                    <div class="tab-pane fade"  id="tasks">
                        <div class="tasks">
                            <div class="">
                                1/5 @lang('app.task')s
                                <a href="" class="ml-10 bold" data-toggle="modal" data-target="#taskcreate">
                                    @lang('app.new_task')
                                </a>
                            </div>


                            {{-- Filter  --}}
                            <div class="mt-10 filter">
                                <h3>Filtre</h3>
                                <form class="form">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <select class="form-control" name="filterby">
                                                    <option value="date">Date</option>
                                                    <option value="status">Statut</option>
                                                    <option value="status">Utilisateur</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <select class="form-control" name="value">
                                                    <option value="chronologiq">Ordre Chronologique</option>
                                                    <option value="status">Ordre Chronologique inversé</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>


                            {{-- Tasks  --}}
                            <div class="rows">
                                <div class="item">
                                    <div class="task green" data-toggle="modal" data-target="#taskedit">
                                        <div class="task-title">
                                            #10002 - Evaluation preliminaire du coût de developement
                                        </div>
                                        <div class="task-date">
                                            <i class="flaticon-time"></i>
                                            15/07/2017 - 20/07/2017
                                        </div>
                                        <div class="task-cost">
                                            <i class="flaticon-funds"></i>
                                            400 000 FCFA
                                        </div>
                                        <div class="task-team">
                                            <i class="flaticon-users-genders"></i>
                                            3
                                        </div>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="task red">
                                        <div class="task-title">
                                            #10003 - Edition/Impression de l'avis d'appel d'offre
                                        </div>
                                        <div class="task-date">
                                            <i class="flaticon-time"></i>
                                            22/07/2017
                                        </div>
                                        <div class="task-cost">
                                            <i class="flaticon-funds"></i>
                                            200 000 FCFA
                                        </div>
                                        <div class="task-team">
                                            <i class="flaticon-users-genders"></i>
                                            2
                                        </div>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="task">
                                        <div class="task-title">
                                            #10004 - Publier l'annonce par les canaux identifies
                                        </div>
                                        <div class="task-date">
                                            <i class="flaticon-time"></i>
                                            24/07/2017
                                        </div>
                                        <div class="task-cost">
                                            <i class="flaticon-funds"></i>
                                            1000 000 FCFA
                                        </div>
                                        <div class="task-team">
                                            <i class="flaticon-users-genders"></i>
                                            4
                                        </div>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="task">
                                        <div class="task-title">
                                            #10005 - Recepetionner les offres
                                        </div>
                                        <div class="task-date">
                                            <i class="flaticon-time"></i>
                                            01/08/2017 - 22/08/2017
                                        </div>
                                        <div class="task-cost">
                                            <i class="flaticon-funds"></i>
                                            90 000 FCFA
                                        </div>
                                        <div class="task-team">
                                            <i class="flaticon-users-genders"></i>
                                            1
                                        </div>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="task">
                                        <div class="task-title">
                                            #10006 - Selectionner l'offre la moins disante
                                        </div>
                                        <div class="task-date">
                                            <i class="flaticon-time"></i>
                                            22/08/2017
                                        </div>
                                        <div class="task-cost">
                                            <i class="flaticon-funds"></i>
                                            200 000 FCFA
                                        </div>
                                        <div class="task-team">
                                            <i class="flaticon-users-genders"></i>
                                            4
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    {{-- Indicators  --}}
                    <div class="tab-pane fade"  id="indicators">
                        <h3>Evolution Idéale Vs Evolution Actuelle</h3>

                        <div class="mt-20">
                            <div id="chart_div"></div>
                        </div>
                    </div>



                    {{-- Gantt --}}
                    <div class="tab-pane fade"  id="gantt">
                        <div id="gantt"></div>
                    </div>

                </div>
            </div>


        </div>

    </section>

    @include('modals.taskcreate')
    @include('modals.taskedit')
@endsection



@section('js')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
$(document).ready(function () {
    $('.date').datepicker({
        autoclose: true,
        format: "dd/mm/yyyy",
        startDate: 'd',
    })

    google.charts.load('current', {packages: ['corechart', 'line', 'gantt']});
    google.charts.setOnLoadCallback(drawLineColors);
    google.charts.setOnLoadCallback(drawChart);

    function drawLineColors() {
        var data = new google.visualization.DataTable();
        data.addColumn('date', 'Y');
        data.addColumn('number', 'Ideal');
        data.addColumn('number', 'Actual');

        data.addRows([
            [new Date(2017, 8, 15), 100, 100],
            [new Date(2017, 8, 22), 90, 90],
            [new Date(2017, 8, 25), 80, 90],
            [new Date(2017, 8, 27), 70, 80],
            [new Date(2017, 8, 29), 60, 80],
            [new Date(2017, 9, 1), 50, 60],
            [new Date(2017, 9, 3), 40, 50],
            [new Date(2017, 9, 8), 30, 30],
            [new Date(2017, 9, 10), 20, 20],
            [new Date(2017, 9, 14), 10, 10],
            [new Date(2017, 9, 15), 0, 0]
        ]);

        var options = {
            hAxis: {
                title: 'Date'
            },
            vAxis: {
                title: 'Tasks'
            },
            'width':900,
            'height':500,
            colors: ['#333', '#08c']
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }



    function daysToMilliseconds(days) {
        return days * 24 * 60 * 60 * 1000;
    }

    function drawChart() {

        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Task ID');
        data.addColumn('string', 'Task Name');
        data.addColumn('date', 'Start Date');
        data.addColumn('date', 'End Date');
        data.addColumn('number', 'Duration');
        data.addColumn('number', 'Percent 10005');
        data.addColumn('string', 'Dependencies');

        data.addRows([
            ['10002', '10002',new Date(2015, 0, 1), new Date(2015, 0, 5), null,  100,  null],
            ['10003', '10003', null, new Date(2015, 0, 9), daysToMilliseconds(3), 25, '10002, 10006'],
            ['10004', '10004',null, new Date(2015, 0, 7), daysToMilliseconds(1), 20, '10002'],
            ['10005', '10005', null, new Date(2015, 0, 10), daysToMilliseconds(1), 0, '10004,10003'],
            ['10006', '10006', null, new Date(2015, 0, 6), daysToMilliseconds(1), 100, '10002']
        ]);

        var options = {
            height: 275,
            width: 1000
        };

        var chart = new google.visualization.Gantt(document.getElementById('gantt'));

        chart.draw(data, options);
    }

})
</script>
@endsection
