@extends('template.layout')

@section('head')
    <title>@lang('app.new_project')</title>
@endsection



@section('body')
    <section class="page">

        <div class="container-fluid">

            <div class="page-title">
                <h4>@lang('app.project_editing')</h4>
            </div>


            <div class="row form">
                <div class="col-sm-9">
                    <div class="block mt-20">
                        <div class="block-content">

                            <div class="form-group has-feedback has-feedback-left" >
                                <label class="control-label">@lang('app.project_title')</label>
                                <div>
                                    <input type="text"
                                    name="title"
                                    class="form-control input-lg"
                                    value="Developement d'un logiciel de suivi des projets de recherche"
                                    placeholder="@lang('app.project_title')">
                                    <i class="form-control-feedback flaticon-page"></i>
                                </div>
                            </div>

                            {{-- Dates  --}}
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group has-feedback" >
                                        <label class="control-label">@lang('app.start_date')</label>
                                        <div>
                                            <input type="text"
                                            class="form-control input-lg date"
                                            value="15/07/2017"
                                            placeholder="@lang('app.start_date')">
                                            <i class="form-control-feedback flaticon-calendar"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group has-feedback has-feedback-left" >
                                        <label class="control-label">@lang('app.due_date')</label>
                                        <div>
                                            <input type="text"
                                            class="form-control input-lg date"
                                            value="15/09/2017"
                                            placeholder="@lang('app.due_date')">
                                            <i class="form-control-feedback flaticon-calendar"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group has-feedback has-feedback-left" >
                                        <label class="control-label">@lang('app.initial_budget')</label>
                                        <div>
                                            <input type="budget"
                                            class="form-control input-lg"
                                            value="10000000"
                                            placeholder="@lang('app.initial_budget')">
                                            <i class="form-control-feedback flaticon-funds"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <hr>
                            <h3>Indicateurs du projet</h3>
                            <div class="indicators fs-16">
                                <input type="checkbox" name="" value="" checked> Ecart de coût du projet <br>
                                <input type="checkbox" name="" value="" checked> Ecart de durée du projet <br>
                                <input type="checkbox" name="" value=""> Niveau d’avancement de projet <br>
                                <input type="checkbox" name="" value=""> Niveau d’avancement de projet <br>
                                <input type="checkbox" name="" value="" checked> Taux de retard  <br>
                                <input type="checkbox" name="" value=""> Taux des coûts non planifiés <br>
                            </div>

                            <hr>
                            <div class="mt-20">
                                <div class="form-group">
                                    <label>@lang('app.project_objectives')</label>
                                    <textarea name="objectives" class="tiny">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna
                                        aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                        ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                        Duis aute irure dolor in reprehenderit in voluptate velit
                                        esse cillum dolore eu fugiat nulla pariatur.
                                        Excepteur sint occaecat cupidatat non proident,
                                        sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </textarea>
                                </div>
                            </div>

                            <div class="mt-40">
                                <div class="form-group">
                                    <label>@lang('app.expected_results')</label>
                                    <textarea name="results" class="tiny">

                                    </textarea>
                                </div>
                            </div>

                            <div class="mt-40">
                                <div class="form-group">
                                    <label>@lang('app.project_deliverables')</label>
                                    <textarea name="deliverables" class="tiny"></textarea>
                                </div>
                            </div>


                            <div class="mt-40 text-right pb-20">
                                <a href="" class="btn btn-red btn-lg">
                                    <i class="flaticon-bin mr-5"></i>
                                    @lang('app.delete_project')
                                </a>

                                <button type="submit" class="btn btn-greener btn-lg">
                                    <i class="flaticon-check mr-5"></i>
                                    @lang('app.submit_project')
                                </button>
                            </div>
                        </div>
                    </div>
                </div>







                <div class="col-sm-3">
                    <div class="block block-dark mt-20">
                        <div class="block-content">
                            <ul class="list-unstyled fs-15 pb-20">
                                <li>@lang('app.created_at') : 22/06/2017 13:22</li>
                                <li>@lang('app.status') : Brouillon</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>




        </div>

    </section>
@endsection



@section('js')
<script src="/assets/tinymce/tinymce.min.js"></script>
<script>
$(document).ready(function () {
    $('.date').datepicker({
        autoclose: true,
        format: "dd/mm/yyyy",
        startDate: 'd',
    })

    tinymce.init({
        selector: ".tiny",
        theme: "modern",
        relative_urls: false,
        height : 200,
        content_style: ".mce-content-body {font-size:15px;font-family:Arial,sans-serif;}",
        theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px,20px",
        font_size_style_values : "10px,12px,13px,14px,16px,18px,20px",
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons paste textcolor code"
        ],
        toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect"
    })
})
</script>
@endsection
