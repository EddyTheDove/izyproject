@extends('template.layout')

@section('head')
    <title>@lang('app.new_project')</title>
@endsection



@section('body')
    <section class="page">

        <div class="container-fluid">

            <div class="page-title">
                <h4>@lang('app.new_project')</h4>
            </div>

            <form class="form" action="" method="post">
                {{ csrf_field() }}

                <div class="block mt-20">
                    <div class="block-content">

                        <div class="form-group has-feedback has-feedback-left" >
                            <label class="control-label">@lang('app.project_title')</label>
                            <div>
                                <input type="text"
                                name="title"
                                class="form-control input-lg"
                                placeholder="@lang('app.project_title')">
                                <i class="form-control-feedback flaticon-page"></i>
                            </div>
                        </div>

                        {{-- Dates  --}}
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group has-feedback" >
                                    <label class="control-label">@lang('app.start_date')</label>
                                    <div>
                                        <input type="text"
                                        class="form-control input-lg date"
                                        placeholder="@lang('app.start_date')">
                                        <i class="form-control-feedback flaticon-calendar"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group has-feedback has-feedback-left" >
                                    <label class="control-label">@lang('app.due_date')</label>
                                    <div>
                                        <input type="text"
                                        class="form-control input-lg date"
                                        placeholder="@lang('app.due_date')">
                                        <i class="form-control-feedback flaticon-calendar"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group has-feedback has-feedback-left" >
                                    <label class="control-label">@lang('app.initial_budget')</label>
                                    <div>
                                        <input type="budget"
                                        class="form-control input-lg"
                                        placeholder="@lang('app.initial_budget')">
                                        <i class="form-control-feedback flaticon-funds"></i>
                                    </div>
                                </div>
                            </div>
                        </div>



                        {{-- <div class="mt-20">
                            <div class="form-group">
                                <label>@lang('app.project_description')</label>
                                <textarea name="objectives" class="tiny"></textarea>
                            </div>
                        </div> --}}


                        <div class="mt-20 pb-20 text-right">
                            <button type="submit" class="btn btn-greener btn-lg">
                                @lang('app.continue')    <i class="flaticon-right ml-5"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>


        </div>

    </section>
@endsection



@section('js')
<script>
$(document).ready(function () {
    $('.date').datepicker({
        autoclose: true,
        format: "dd/mm/yyyy",
        startDate: 'd',
    })
})
</script>
@endsection
