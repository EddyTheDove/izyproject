@extends('template.layout')

@section('head')
    <title>Indicateurs</title>
@endsection



@section('body')
    <section class="dashboard">

        <div class="container-fluid">

            <div class="">
                <h4 class="title">Indicateurs</h4>

                <table class="table table-custom">
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Statut</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr data-href="">
                            <td>Ecart de coût du projet </td>
                            <td>Actif</td>
                        </tr>
                        <tr data-href="">
                            <td>Ecart de durée du projet</td>
                            <td>Actif</td>
                        </tr>
                        <tr data-href="">
                            <td>Niveau d’avancement de projet </td>
                            <td>Actif</td>
                        </tr>
                        <tr data-href="">
                            <td> Taux de retard</td>
                            <td>Actif</td>
                        </tr>
                        <tr data-href="">
                            <td>Marge moyenne des tâches</td>
                            <td>Actif</td>
                        </tr>
                    </tbody>
                </table>

            </div>

        </div>

    </section>
@endsection
