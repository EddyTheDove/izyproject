<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
    <title>@lang('app.sign_in')</title>
    <link rel="stylesheet" href="/assets/css/app.min.css">
    <link rel="icon" type="image/png"  href="/favicon.ico">
</head>
<body>
    <section class="login-page">

        <div class="login-container">
            <div class="inner">
                <div class="logo"></div>
                <div class="logo-name">IzyProject</div>



                <div class="form-container">
                    <h4>@lang('app.sign_in')</h4>

                    <form class="form" action="" method="post">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <input type="email"
                                name="email"
                                placeholder="@lang('app.email')"
                                class="form-control input-lg"
                                required>
                        </div>

                        <div class="form-group">
                            <input type="password"
                                name="password"
                                placeholder="@lang('app.password')"
                                class="form-control input-lg"
                                required>
                        </div>


                        <div class="mt-10 text-center">
                            <button type="submit" class="btn btn-lg btn-block btn-greener uppercase">
                                @lang('app.sign_in')
                            </button>
                        </div>
                    </form>
                </div>

                <div class="links">
                    <ul class="list-unstyled">
                        <li>
                            <a href="">
                                @lang('app.forgot_password')
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</body>
</html>
