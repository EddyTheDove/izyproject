@extends('template.layout')

@section('head')
    <title>@lang('app.my_tasks')</title>
@endsection



@section('body')
    <section class="dashboard">

        <div class="container-fluid">

            <div class="">
                <h4 class="title">@lang('app.my_tasks')</h4>

                <table class="table table-custom">
                    <colgroup>
                        <col class="col-sm-8">
                        <col class="col-sm-2">
                        <col class="col-sm-2">
                        <col class="col-sm-2">
                    </colgroup>
                    <thead>
                        <tr>
                            <th>@lang('app.project')</th>
                            <th>@lang('app.project_manager')</th>
                            <th>@lang('app.due_date')</th>
                            <th>@lang('app.task')s</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($projects as $p)
                            <tr data-href="/projects/6a5d78c2-37eb-4285-86d3-e4d124fc451d">
                                <td>
                                    {{ $p->title }}
                                </td>
                                <td>{{ $p->manager }}</td>
                                <td>{{ $p->date }}</td>
                                <td>{{ $p->tasks }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>

        </div>

    </section>
@endsection
