@extends('template.layout')

@section('head')
    <title>Mon Compte</title>
@endsection



@section('body')

    <section class="page">

        <div class="container-fluid">
            <div class="page-title">
                <h4>Mon Compte</h4>
            </div>

            <div class="row">
                <div class="col-sm-9 col-md-8">
                    <div class="block">
                        <div class="block-content">
                            <form class="form">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group has-feedback has-feedback-left" >
                                            <label class="control-label">Prénom</label>
                                            <div>
                                                <input type="text"
                                                name="title"
                                                class="form-control input-lg"
                                                value="Max">
                                                <i class="form-control-feedback flaticon-user-material"></i>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group has-feedback has-feedback-left" >
                                            <label class="control-label">Nom</label>
                                            <div>
                                                <input type="text"
                                                name="name"
                                                class="form-control input-lg"
                                                value="Emmanuel">
                                                <i class="form-control-feedback flaticon-user-material"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group has-feedback has-feedback-left" >
                                    <label class="control-label">Email</label>
                                    <div>
                                        <input type="text"
                                        name="email"
                                        class="form-control input-lg"
                                        value="max.emmanuel@email.com">
                                        <i class="form-control-feedback flaticon-mail"></i>
                                    </div>
                                </div>


                                <div class="form-group has-feedback has-feedback-left" >
                                    <label class="control-label">Position</label>
                                    <div>
                                        <input type="text"
                                        name="position"
                                        class="form-control input-lg"
                                        value="Project Manager">
                                        <i class="form-control-feedback flaticon-suitcase"></i>
                                    </div>
                                </div>


                                <div class="mt-10 pb-20">
                                    <button type="submit" class="btn btn-lg btn-greener">
                                        <i class="flaticon-check"></i> Sauvegarder
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>





        <div class="container-fluid">
            <div class="page-title">
                <h4>Sécurité</h4>
            </div>

            <div class="row">
                <div class="col-sm-9 col-md-8">
                    <div class="block">
                        <div class="block-content">
                            <form class="form">

                                <div class="form-group has-feedback has-feedback-left" >
                                    <label class="control-label">Mot de passe actuel</label>
                                    <div>
                                        <input type="password"
                                        name="password"
                                        class="form-control input-lg"
                                        placeholder="Mot de passe actuel">
                                        <i class="form-control-feedback flaticon-lock"></i>
                                    </div>
                                </div>


                                <div class="form-group has-feedback has-feedback-left" >
                                    <label class="control-label">Nouveau mot de passe</label>
                                    <div>
                                        <input type="password"
                                        name="password"
                                        class="form-control input-lg"
                                        placeholder="Nouveau mot de passe">
                                        <i class="form-control-feedback flaticon-lock"></i>
                                    </div>
                                </div>

                                <div class="form-group has-feedback has-feedback-left" >
                                    <label class="control-label">Confirmer le mot de passe</label>
                                    <div>
                                        <input type="password"
                                        name="password"
                                        class="form-control input-lg"
                                        placeholder="Confirmer le mot de passe">
                                        <i class="form-control-feedback flaticon-lock"></i>
                                    </div>
                                </div>

                                <div class="mt-10 pb-20">
                                    <button type="submit" class="btn btn-lg btn-blue">
                                        <i class="flaticon-check"></i> Sauvegarder
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection
