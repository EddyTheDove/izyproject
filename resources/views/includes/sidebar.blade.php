<div id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li class="brand">
            <a  href="/">
                IZYPROJECT
            </a>
        </li>

        <li class="{{ Request::is('dashboard') ? 'active' : '' }}">
            <a href="/dashboard">
                <i class="flaticon-desktop"></i>
                @lang('app.dashboard')
            </a>
        </li>

        <li class="{{ Request::is('my-projects*') ? 'active' : '' }}">
            <a href="/my-projects">
                <i class="flaticon-folder"></i>
                @lang('app.my_projects')
            </a>
        </li>

        <li class="{{ Request::is('my-tasks*') ? 'active' : '' }}">
            <a href="/my-tasks">
                <i class="flaticon-layers"></i>
                @lang('app.my_tasks')
            </a>
        </li>

        <li class="{{ Request::is('users*') ? 'active' : '' }}">
            <a href="/users">
                <i class="flaticon-users-genders"></i>
                @lang('app.user')s
            </a>
        </li>

        <li class="{{ Request::is('projets*') ? 'active' : '' }}">
            <a href="/projects">
                <i class="flaticon-posts"></i>
                @lang('app.projets_management')
            </a>
        </li>

        <li class="{{ Request::is('indicators*') ? 'active' : '' }}">
            <a href="/indicators">
                <i class="flaticon-slides-h"></i>
                @lang('app.indicator')s
            </a>
        </li>

        <li class="{{ Request::is('units*') ? 'active' : '' }}">
            <a href="/units">
                <i class="flaticon-building"></i>
                @lang('app.org_units')
            </a>
        </li>

        <li class="separer"></li>

        <li>
            <a href="/account">
                <i class="flaticon-user-material"></i>
                Mon Compte
            </a>
        </li>

        <li>
            <a href="/logout">
                <i class="flaticon-power"></i>
                Déconnexion
            </a>
        </li>
    </ul>
</div>
