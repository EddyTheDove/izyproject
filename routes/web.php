<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('login', 'front\AuthController@login');
Route::post('login', 'front\AuthController@signin');


Route::get('units', 'front\UnitController@index');
Route::get('dashboard', 'front\DashboardController@index');
Route::get('my-tasks', 'front\TaskController@userTasks');
Route::get('my-projects', 'front\ProjectController@userProjects');
Route::get('indicators', 'front\IndicatorController@index');
Route::get('account', 'front\UserController@account');

Route::group(['prefix' => 'projects'], function () {
    Route::get('create', 'front\ProjectController@create')->name('projects.create');
    Route::post('create', 'front\ProjectController@store')->name('projects.store');
    Route::get('{code}', 'front\ProjectController@show')->name('projects.show');
    Route::get('{code}/edit', 'front\ProjectController@edit')->name('projects.edit');
});
