<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    public function index ()
    {
        $projects = [
            (object)[
                'title' => "Developement d'une application android pour la vulgarisation des informations sur les MST",
                'manager' => 'Max Emmanuel',
                'date'  => '22/02/2018',
                'status' => 'app.reviewing'
            ],
            (object)[
                'title' => "Achat des vehicules de service",
                'manager' => 'Armel Fotsoh',
                'date'  => '15/01/2017',
                'status' => 'app.in_progress'
            ],
            (object)[
                'title' => "Logiciel de suivi de projets",
                'manager' => 'Max Emmanuel',
                'date'  => '30/12/2017',
                'status' => 'app.in_progress'
            ],
            (object)[
                'title' => "Logiciel de maintenace à distance des ordinateurs",
                'manager' => 'Armel Fotsoh',
                'date'  => '10/12/2017',
                'status' => 'app.in_progress'
            ],
            (object)[
                'title' => "Logiciel de gestion des Ressources Humaines",
                'manager' => 'Max Emmanuel',
                'date'  => '10/11/2017',
                'status' => 'app.in_progress'
            ]
        ];
        return view('dashboard.index', compact('projects'));
    }
}
