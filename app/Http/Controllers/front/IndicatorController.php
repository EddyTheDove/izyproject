<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndicatorController extends Controller
{
    public function index ()
    {
        return view('indicators.index');
    }
}
