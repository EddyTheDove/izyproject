<?php

namespace App\Http\Controllers\front;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{

    /**
     * Display login page
     * @return [type] [description]
     */
    public function login ()
    {
        return view('auth.login');
    }



    /**
     * Authenticate the user
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function signin (Request $request)
    {
        return redirect()->to('/dashboard');
    }
}
