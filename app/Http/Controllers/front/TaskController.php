<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{

    /**
     * Show a single user's tasks
     *
     * Select projects where user is involved
     *
     * Count the number of taks assigned to the user per project
     *
     * @return View view
     */
    public function userTasks()
    {
        $projects = [
            (object)[
                'title' => "Developement d'une application android pour la vulgarisation des informations sur les MST",
                'manager' => 'Max Emmanuel',
                'date'  => '22/02/2018',
                'tasks' => '0/4'
            ],
            (object)[
                'title' => "Achat des vehicules de service",
                'manager' => 'Armel Fotsoh',
                'date'  => '15/01/2017',
                'tasks' => '6/8'
            ],
            (object)[
                'title' => "Logiciel de suivi de projets",
                'manager' => 'Max Emmanuel',
                'date'  => '30/12/2016',
                'tasks' => '10/10'
            ],
            (object)[
                'title' => "Logiciel de maintenace à distance des ordinateurs",
                'manager' => 'Armel Fotsoh',
                'date'  => '10/12/2016',
                'tasks' => '5/5'
            ]
        ];

        return view('users.tasks', compact('projects'));
    }
}
