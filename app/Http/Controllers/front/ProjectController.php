<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{

    public function create ()
    {
        return view('projects.create');
    }



    /**
     * Save a project as a draft
     * Redirect to the project edit page
     *
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store (Request $request)
    {
        return redirect()->route('projects.edit', '6a5d78c2-37eb-4285-86d3-e4d124fc451d');
    }


    /**
     * Show single project
     * @param  [type] $code [description]
     * @return [type]       [description]
     */
    public function show($code)
    {
        return view('projects.show');
    }



    /**
     * Edit the project
     *
     * Get the project by its code (uuid)
     *
     * @param  [type] $code [description]
     * @return [type]       [description]
     */
    public function edit($code)
    {
        return view('projects.edit');
    }




    /**
     * Update a project
     * Redirect to the project edit page
     *
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function update (Request $request, $code)
    {
        return redirect()->back()->with('message', trans('app.project_updated'));
    }


    /**
     * User's projects
     *
     * @return [type] [description]
     */
    public function userProjects ()
    {
        $projects = [
            (object)[
                'title' => "Developement d'une application android pour la vulgarisation des informations sur les MST",
                'manager' => 'Max Emmanuel',
                'date'  => '22/02/2018',
                'status' => 'app.draft'
            ],
            (object)[
                'title' => "Achat des vehicules de service",
                'manager' => 'Armel Fotsoh',
                'date'  => '15/01/2017',
                'status' => 'app.reviewing'
            ],
            (object)[
                'title' => "Logiciel de suivi de projets",
                'manager' => 'Max Emmanuel',
                'date'  => '30/12/2016',
                'status' => 'app.in_progress'
            ],
            (object)[
                'title' => "Logiciel de maintenace à distance des ordinateurs",
                'manager' => 'Armel Fotsoh',
                'date'  => '10/12/2016',
                'status' => 'app.in_progress'
            ],
            (object)[
                'title' => "Logiciel de gestion des Ressources Humaines",
                'manager' => 'Max Emmanuel',
                'date'  => '10/11/2016',
                'status' => 'app.in_progress'
            ]
        ];
        return view('users.projects', compact('projects'));
    }
}
