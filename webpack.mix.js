const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.js('resources/assets/js/app.js', 'public/assets/js/app.min.js')
.extract(['vue', 'jquery', 'bootstrap-sass'])
.scripts([
    'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'
], 'public/assets/js/scripts.min.js');
// .sass('resources/assets/sass/app.scss', 'public/assets/css/app.min.css');
